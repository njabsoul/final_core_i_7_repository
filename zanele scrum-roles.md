#Scrum roles and responsibilities
*Coach
    *The scrum must lead the team and coach both development team and the product owner thus            eliminating any barriers that may occur between the two parties
    *He must ensure that the teams performs, delivers and improves all the time
    *He doesn’t necessarily solve problems. But help with the team to solve the impediments they        may face
    *He must at all times ensure that the spirit of the team is always high 
*Servant Leader
    *He lives to ensure that the team’s priorities are met all the time, and always offers the team      help, instead of asking the team for the way forward
*Process Authority
    *He must ensure that the team sticks to the team’s values and principles
*Interference shield
    *He must always protect the team from external factors that may disturb the team’s progress
*Impediment Remover
    *When the team gets impediments they can’t deal with, the scrum master is must take the              initiative to ensure that the impediments are resolved
*Change agent
    *When the team encounters changes(strategic and role rotation) the scrum master ensures that        the team adjusts well to the changes, and that the changes do not affect the productivity of        the team negatively

